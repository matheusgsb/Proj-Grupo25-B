#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct {
    unsigned char R;
    unsigned char G;
    unsigned char B;
} pixel;

typedef struct {
    int lin;
    int col;
    int max;
    pixel *pixels;
} imagem;

/* Aloca espaco na memoria para uma imagem com 'lin' linhas e 'col' colunas */
imagem *alocaImagem(int lin, int col, int max) {

    imagem *img = (imagem *)malloc(sizeof(imagem));
    if (!img)
        return NULL;
        
    img->lin = lin;
    img->col = col;
    img->max = max;
    img->pixels = (pixel *)malloc(lin * col * sizeof(pixel));
    if (!img->pixels)
        return NULL;
        
    return img;

}

/* Libera o espaco alocado para a imagem */
void liberaImagem(imagem *img) {
    if (img != NULL) {
        free(img->pixels);
        free(img);
    }
}

/* Simula o acesso de uma matriz, retornando o pixel na posicao (i,j) */
pixel obterPixel(imagem *img, int i, int j) {
    
    return img->pixels[(img->col * i) + j];    
  
}

/* Altera o pixel (i,j) na imagem */
void salvarPixel(imagem *img, int i, int j, unsigned char R, unsigned char G, unsigned char B) {

    img->pixels[(img->col * i) + j].R = R;
    img->pixels[(img->col * i) + j].G = G;
    img->pixels[(img->col * i) + j].B = B;

}

/* Verifica se a imagem e PPM e esta no formato P6 */
int testaFormatoImagem(FILE *arquivo) {

    char c;
    char comentarios[1024];
    
    c = getc(arquivo);
    if (c=='P' || c=='p') {
        c = getc(arquivo);
        if (c=='6') {
            c = getc(arquivo);
            if (c=='\n' ||c=='\r') {
			    c = getc(arquivo);
			    while(c=='#') {
				    fscanf(arquivo, "%[^\n\r] ", comentarios);
				    c = getc(arquivo);
			    }
			    ungetc(c,arquivo); 
		    }
		    return 1;
        } else 
            return 0;
    } else 
        return 0;

}

/* Carrega a imagem do arquivo para a estrutura alocada */
imagem *carregarImagemPPM(FILE *arquivo) {

    int lin, col, max;

    if (!testaFormatoImagem(arquivo)) {
        printf("\n - Erro: Imagem não está no formato .PPM P6 \n\n"); 
        return NULL;
    }
    
    /* Obtem o width e o height da imagem, e o valor maximo de cada componente RGB */
    fscanf(arquivo, "%d %d %d", &col, &lin, &max);
    
    imagem *img = alocaImagem(lin, col, max);
    
    /* Le os pixels da imagem */
    size_t tamanho = fread(img->pixels, sizeof(pixel), lin * col, arquivo);
    if ( tamanho < lin * col ) {
        liberaImagem(img);
        return NULL;
    }
    
    return img;
}

/* Salva a imagem em um arquivo .ppm */
void salvarImagem(imagem *img, char *nomeSaida) {

    int i;
    unsigned char cor[3];
    
    FILE *saida = fopen(nomeSaida,"wb");
    if (!saida) {
        printf("\n - Erro ao gerar o arquivo de saida. \n\n");
        return;
    }
    
    fprintf(saida, "P6\n%d %d\n%d",img->col, img->lin, img->max);
    for (i=0;i< img->lin * img->col ;i++) {
        cor[0] = img->pixels[i].R;
        cor[1] = img->pixels[i].G;
        cor[2] = img->pixels[i].B;
        fwrite(cor, 1, 3, saida);
    }
    
    fclose(saida);
}

/* Aplica o filtro smooth na imagem original e salva em uma nova imagem */
int filtroSmooth(imagem *original, imagem *nova) {

    int i, j, k, l;
    int numeroPixels;
    int somaR, somaG, somaB;
    
    if (!original || !nova)
        return 0;
    
    for (i=0 ; i < (original->lin) ; i++) {
        for (j= 0 ; j< (original->col) ; j++) {

            /* Para cada pixel, analisa os pixels ao seu redor (5x5) */
            numeroPixels = 25;            
            somaR = 0;
            somaG = 0;
            somaB = 0;
            
            for (k=(i-2) ; k<=(i+2) ; k++) {
                for (l=(j-2) ; l<=(j+2) ; l++) {
            
                    if ((k<0) || (k>=original->lin) || (l<0) || (l>=original->col)) {	
	                    somaR += (obterPixel(original,i,j)).R ;
	                    somaG += (obterPixel(original,i,j)).G ;
	                    somaB += (obterPixel(original,i,j)).B ;
                    } else {   
                        somaR += (obterPixel(original,k,l)).R ;
                        somaG += (obterPixel(original,k,l)).G ;
                        somaB += (obterPixel(original,k,l)).B ;
                    }
                }
            }
            
             salvarPixel(nova, i, j, somaR / numeroPixels, 
                                     somaG / numeroPixels, 
                                     somaB / numeroPixels);
            
        }
    }
    
    /* Copia os pixels de borda da imagem original */
    
    for (i=0 ; i <= 1 ; i++) 
        for (j=0 ; j < original->col ; j++) {
            salvarPixel(nova,i,j,
                                 (obterPixel(original,i,j)).R, 
                                 (obterPixel(original,i,j)).G, 
                                 (obterPixel(original,i,j)).B);
            salvarPixel(nova,(original->lin - i) -1 ,j,
                                 (obterPixel(original,(original->lin - i) -1,j)).R, 
                                 (obterPixel(original,(original->lin - i) -1,j)).G, 
                                 (obterPixel(original,(original->lin - i) -1,j)).B);
        }
    
    for (j=0 ; j <= 1 ; j++)
        for (i=0 ; i < original->lin ; i++) {
            salvarPixel(nova,i,j,
                                 (obterPixel(original,i,j)).R, 
                                 (obterPixel(original,i,j)).G, 
                                 (obterPixel(original,i,j)).B);
            salvarPixel(nova, i ,(original->col - j) -1 ,
                                 (obterPixel(original,i,(original->col - j) -1)).R, 
                                 (obterPixel(original,i,(original->col - j) -1)).G, 
                                 (obterPixel(original,i,(original->col - j) -1)).B);
        } 
    
    return 1;

}


int main(int argc, char **argv) {
    
    imagem *imgOriginal;
    imagem *imgNova;
    
    FILE *arquivo;
    char *nomeDoArquivo;
    char *arquivoSaida;
    
    int lin, col, max, i, j, R, G, B;

    struct timeval start, end;
    double time_spent;    

    /* Nome do Arquivo */
    if (argc > 1) {
        nomeDoArquivo = (char *)malloc(sizeof(argv[1]));
        strcpy(nomeDoArquivo, argv[1]);
    } else {
        nomeDoArquivo = (char *)malloc(7 * sizeof(char));
        strcpy(nomeDoArquivo, "in.ppm");
    }

    /* Tenta abrir a imagem */

    gettimeofday(&start, NULL);

    arquivo = fopen(nomeDoArquivo, "rb");
    if (!arquivo) {
        printf("\n - Erro ao abrir a imagem %s. Programa abortado.\n\n",nomeDoArquivo);
        return 1;
    }
    
    imgOriginal = carregarImagemPPM(arquivo);
    if (!imgOriginal) {
        printf("\n - Erro ao carregar a imagem. \n\n");
        return 1;
    }
    
    imgNova = alocaImagem(imgOriginal->lin, imgOriginal->col, imgOriginal->max);
    if (!imgNova) {
        printf("\n - Erro ao carregar a imagem \n\n");
        return 1;
    }
    
    gettimeofday(&end, NULL);
    time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;    
    printf("\n - Tempo de leitura e armazenamento da imagem: %lfs",time_spent);
    

    gettimeofday(&start, NULL);
    
    if (filtroSmooth(imgOriginal, imgNova)) {
    
        gettimeofday(&end, NULL);
        time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
        printf("\n - Tempo para aplicar o filtro: %lfs",time_spent);
    
	    if (argc > 2) {
            arquivoSaida = (char *)malloc(sizeof(argv[2]));
            strcpy(arquivoSaida, argv[2]);
        } else {
            arquivoSaida = (char *)malloc(8 * sizeof(char));
            strcpy(arquivoSaida, "out.ppm");
        }
        
        gettimeofday(&start, NULL);    
        
        salvarImagem(imgNova, arquivoSaida);
        
        gettimeofday(&end, NULL);
        time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
        printf("\n - Tempo para salvar a imagem: %lfs \n\n",time_spent);
    
    
    } else {
        printf("\n - Erro ao aplicar o filtro \n\n");
        return 1;    
    }

    liberaImagem(imgNova);
    liberaImagem(imgOriginal);  
    
    return 0;

    
}
