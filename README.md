Este documento apresenta os passos para execução do algoritmo sequencial e paralelos, disponíveis em https://gitlab.com/matheusgsb/Proj-Grupo25-B . 
<br /><br />
Para rodar o algoritmo sequencial, seguir os passos:

1) Abra o terminal no local que contém o arquivo sequencial.c 
<br />2) Para compilar:	gcc sequencial.c -o sequencial
<br />3) Para executar:
<br />    a) ./sequencial                 	    (lê da imagem in.ppm)
<br />    b) ./sequencial entrada.ppm     		(lê da imagem entrada.ppm fornecida)
<br />    c) ./sequencial entrada.ppm saida.ppm   (lê da imagem entrada.ppm e salva na imagem saida.ppm)
<br /><br />
Para rodar o primeiro algoritmo paralelo, consideramos que você já tenha instalado em sua máquina openMPI (disponível em: http://www.open-mpi.org/software/ompi/v1.10/).
<br /><br />Caso tenha alguma dúvida na instalação, seguir os passos contidos no link a seguir:  http://www.itp.phys.ethz.ch/education/hs12/programming_techniques/openmpi.pdf
<br /><br />Em seguida, realizar os passos abaixo:

1) Abra o terminal no local que contém o arquivo paralelo.c
<br />2) Para compilar:	mpicc -fopenmp paralelo.c -o paralelo
<br />3) Para executar:
<br />    a) mpirun -np 4 ./paralelo    		        (para executar com 4 processos)
<br />    b) mpirun -hostfile hosts10 -np 4 ./paralelo	(para incluir uma lista de nós)
<br />    c) mpirun -np 4 ./paralelo entrada.ppm saida.ppm     (para especificar entrada e saída)
<br />
<br />
Para rodar o algoritmo paralelo que utiliza CUDA, consideramos que você tenha uma placa gráfica com suporte a CUDA e todos os componentes necessários para a utilização desta tecnologia.
<br /><br />Em seguida, realizar os passos abaixo:

1) Abra o terminal no local que contém o arquivo cuda.cu
<br />2) Para compilar:	nvcc cuda.cu -o cuda
<br />3) Para executar:
<br />    a) ./cuda                 	    (lê da imagem in.ppm)
<br />    b) ./cuda entrada.ppm     		(lê da imagem entrada.ppm fornecida)
<br />    c) ./cuda entrada.ppm saida.ppm   (lê da imagem entrada.ppm e salva na imagem saida.ppm)

<br />
Para as imagens em tons de cinza, favor utilizar imagens PGM P5 e os arquivos sequencial_gs.c, paralelo_gs.c e cuda_gs.c de forma análoga.