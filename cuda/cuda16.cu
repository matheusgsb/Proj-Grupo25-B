#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

typedef struct {
    unsigned char R;
    unsigned char G;
    unsigned char B;
} pixel;

typedef struct {
    int lin;
    int col;
    int max;
    pixel *pixels;
} imagem;

/* Aloca espaco na memoria para uma imagem com 'lin' linhas e 'col' colunas */
imagem *alocaImagem(int lin, int col, int max) {

    imagem *img = (imagem *)malloc(sizeof(imagem));
    if (!img)
        return NULL;
        
    img->lin = lin;
    img->col = col;
    img->max = max;
    img->pixels = (pixel *)malloc(lin * col * sizeof(pixel));
    if (!img->pixels)
        return NULL;
        
    return img;

}

/* Libera o espaco alocado para a imagem */
void liberaImagem(imagem *img) {
    if (img != NULL) {
        free(img->pixels);
        free(img);
    }
}

/* Verifica se a imagem e PPM e esta no formato P6 */
int testaFormatoImagem(FILE *arquivo) {

    char c;
    char comentarios[1024];
    
    c = getc(arquivo);
    if (c=='P' || c=='p') {
        c = getc(arquivo);
        if (c=='6') {
            c = getc(arquivo);
            if (c=='\n' ||c=='\r') {
			    c = getc(arquivo);
			    while(c=='#') {
				    fscanf(arquivo, "%[^\n\r] ", comentarios);
				    c = getc(arquivo);
			    }
			    ungetc(c,arquivo); 
		    }
		    return 1;
        } else 
            return 0;
    } else 
        return 0;

}

/* Carrega a imagem do arquivo para a estrutura alocada */
imagem *carregarImagemPPM(FILE *arquivo) {

    int lin, col, max;

    if (!testaFormatoImagem(arquivo)) {
        printf("\n - Erro: Imagem não está no formato .PPM P6 \n\n"); 
        return NULL;
    }
    
    /* Obtem o width e o height da imagem, e o valor maximo de cada componente RGB */
    fscanf(arquivo, "%d %d %d", &col, &lin, &max);
    
    imagem *img = alocaImagem(lin, col, max);
    
    /* Le os pixels da imagem */
    size_t tamanho = fread(img->pixels, sizeof(pixel), lin * col, arquivo);
    if ( tamanho < lin * col ) {
        liberaImagem(img);
        return NULL;
    }
    return img;
}

/* Salva a imagem em um arquivo .ppm */
void salvarImagem(imagem *img, char *nomeSaida) {

    int i;
    unsigned char cor[3];
    
    FILE *saida = fopen(nomeSaida,"wb+");
    if (!saida) {
        printf("\n - Erro ao gerar o arquivo de saida. \n\n");
        return;
    }
    
    fprintf(saida, "P6\n%d %d\n%d",img->col, img->lin, img->max);
    for (i=0;i< img->lin * img->col ;i++) {
        cor[0] = img->pixels[i].R;
        cor[1] = img->pixels[i].G;
        cor[2] = img->pixels[i].B;
        fwrite(cor, 1, 3, saida);
    }
    
    fclose(saida);
}

/* Aplica o filtro smooth na imagem original e salva em uma nova imagem */
__global__ void filtroSmooth(pixel *original, pixel *nova, int lin, int col) {

    int i, j, k, l;
    int numeroPixels;
    int somaR, somaG, somaB;
    
    if (!original || !nova)
        return;
    
    i = threadIdx.y + blockDim.y * blockIdx.y;
    j = threadIdx.x + blockDim.x * blockIdx.x;
    
    if (i<0 || i>lin || j<0 || j>col)
      return;
    
    if (i<2 || i> (col-2) || j<2 || j> (lin-2)) {
    
      nova[col*i + j].R = original[col*i + j].R;
      nova[col*i + j].G = original[col*i + j].G;
      nova[col*i + j].B = original[col*i + j].B;

    } else {
    
      /* Para cada pixel, analisa os pixels ao seu redor (5x5) */
      numeroPixels = 25;            
      somaR = 0;
      somaG = 0;
      somaB = 0;
    
      for (k=(i-2) ; k<=(i+2) ; k++) {
          for (l=(j-2) ; l<=(j+2) ; l++) {
    
              if ((k<=0) || (k>=lin) || (l<=0) || (l>col)) {	
                somaR += original[col*i + j].R ;
                somaG += original[col*i + j].G ;
                somaB += original[col*i + j].B ;
              } else {   
                  somaR += original[col*k + l].R ;
                  somaG += original[col*k + l].G ;
                  somaB += original[col*k + l].B ;
              }
          }
      }
    
      nova[col*i + j].R = somaR / numeroPixels;
      nova[col*i + j].G = somaG / numeroPixels;
      nova[col*i + j].B = somaB / numeroPixels;
    
    }
    
}


int main(int argc, char **argv) {
    
    imagem *imgOriginal;
    imagem *imgNova;
    
    FILE *arquivo;
    char *nomeDoArquivo;
    char *arquivoSaida;
    
    int tamanho;
    int nThreads, nBlocos_X, nBlocos_Y;

    struct timeval start, end;
    double time_spent;    
    
    pixel *dev_original, *dev_nova;

    /* Nome do Arquivo */
    if (argc > 1) {
        nomeDoArquivo = (char *)malloc(sizeof(argv[1]));
        strcpy(nomeDoArquivo, argv[1]);
    } else {
        nomeDoArquivo = (char *)malloc(7 * sizeof(char));
        strcpy(nomeDoArquivo, "in.ppm");
    }

    /* Tenta abrir a imagem */

    gettimeofday(&start, NULL);

    arquivo = fopen(nomeDoArquivo, "rb");
    if (!arquivo) {
        printf("\n - Erro ao abrir a imagem %s. Programa abortado.\n\n",nomeDoArquivo);
        return 1;
    }
    
    imgOriginal = carregarImagemPPM(arquivo);
    if (!imgOriginal) {
        printf("\n - Erro ao carregar a imagem. \n\n");
        return 1;
    }
    
    imgNova = alocaImagem(imgOriginal->lin, imgOriginal->col, imgOriginal->max);
    if (!imgNova) {
        printf("\n - Erro ao carregar a imagem \n\n");
        return 1;
    }
    
    gettimeofday(&end, NULL);
    time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;    
    printf("\n - Tempo de leitura e armazenamento da imagem: %lfs",time_spent);

    tamanho = imgOriginal->lin * imgOriginal->col;

    //Aloca memória na GPU
    cudaMalloc(&dev_original, tamanho * sizeof(pixel));
    cudaMalloc(&dev_nova, tamanho * sizeof(pixel));
  
    //Copia para a memória da GPU  
    cudaMemcpy(dev_original,imgOriginal->pixels,tamanho * sizeof(pixel),cudaMemcpyHostToDevice);	

    nThreads = 16;
    nBlocos_X = ceil((float)imgOriginal->col/nThreads);
    nBlocos_Y = ceil((float)imgOriginal->lin/nThreads);

    dim3 tamGrid(nBlocos_X,nBlocos_Y,1);
    dim3 tamBloco(nThreads,nThreads,1);

    gettimeofday(&start, NULL);

    filtroSmooth<<<tamGrid,tamBloco>>>(dev_original, dev_nova, imgOriginal->lin, imgOriginal->col);    
    
    gettimeofday(&end, NULL);
    time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
    printf("\n - Tempo para aplicar o filtro: %lfs",time_spent);
    
    cudaMemcpy(imgNova->pixels, dev_nova, tamanho * sizeof(pixel), cudaMemcpyDeviceToHost);
    
    if (argc > 2) {
          arquivoSaida = (char *)malloc(sizeof(argv[2]));
          strcpy(arquivoSaida, argv[2]);
    } else {
        arquivoSaida = (char *)malloc(8 * sizeof(char));
        strcpy(arquivoSaida, "out.ppm");
    }
    
    gettimeofday(&start, NULL);    
    
    salvarImagem(imgNova, arquivoSaida);
    
    gettimeofday(&end, NULL);
    time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
    printf("\n - Tempo para salvar a imagem: %lfs \n\n",time_spent);

    liberaImagem(imgNova);
    liberaImagem(imgOriginal);  

    cudaFree(dev_original);
	  cudaFree(dev_nova);
    
    return 0;

    
}
